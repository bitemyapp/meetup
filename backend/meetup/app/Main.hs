{-# LANGUAGE TypeOperators #-}

module Main where

import qualified Control.Concurrent.Async as Async
import qualified Data.ByteString.Char8 as BC8
import Data.Maybe (fromMaybe)
import Data.Pool (Pool)
import Data.Proxy
import qualified Data.String.Class as S
import Data.Text (Text)
import qualified Database.Persist.Postgresql as P
import Elm.Module
import Elm.Versions
import qualified Meetup.ApiTypes as AT
import Meetup.Handlers
import Meetup.Model
import Meetup.StubData
import Meetup.Types
import qualified Network.HTTP.Types.Status as HTTPStatus
import Network.Wai (Request)
import qualified Network.Wai as Wai
import qualified Network.Wai.Handler.Warp as Warp
import qualified Network.Wai.Handler.WarpTLS as WarpTLS
import RIO
import Safe (readMay)
import Servant
import Servant.API.Generic ((:-), ToServantApi, genericApi)
import Servant.Server.Experimental.Auth (AuthHandler)
import Servant.Server.Generic (AsServerT, genericServerT)
import System.Environment (getArgs, lookupEnv)
import qualified WaiAppStatic.Storage.Filesystem as WAS
import Web.Cookie (SetCookie)

defaultHost :: ByteString
defaultHost = "meetup.kyiv.ua"

data API route = API
  { _index :: route :- Get '[ PlainText] Text
  , _ping :: route :- "api" :> "ping" :> Get '[ PlainText] Text
  , _meetupsList :: route :- "api" :> "meetups.json" :> Get '[ JSON] [AT.Meetup]
  , _meetupInfo :: route :- "api" :> "meetup" :> Capture "meetup-id" (Key Meetup) :> "info.json" :> Get '[ JSON] AT.Meetup
  , _eventsList :: route :- "api" :> "meetup" :> Capture "meetup-id" (Key Meetup) :> "events.json" :> Get '[ JSON] [AT.Event]
  , _accountInfo :: route :- AuthProtect "cookie-auth" :> "api" :> "account-info.json" :> Get '[ JSON] AT.AccountInfo
  , _logInSendPassword :: route :- "api" :> "log-in-send-password" :> ReqBody '[ JSON] AT.LogInSendPasswordForm :> Post '[ JSON] ()
  , _logInSendCode :: route :- "api" :> "log-in" :> ReqBody '[ JSON] AT.LogInSendCodeForm :> Post '[ JSON] (Headers '[ Header "Set-Cookie" SetCookie] AT.AccountInfo)
  , _updateAccount :: route :- AuthProtect "cookie-auth" :> "api" :> "account" :> "update.json" :> ReqBody '[ JSON] AT.UpdateAccountForm :> Post '[ JSON] AT.AccountInfo
  , _rsvpList :: route :- "api" :> "event" :> Capture "event-id" (Key Event) :> "rsvp" :> "list.json" :> Get '[ JSON] [AT.RsvpInfo]
  , _rsvpCreate :: route :- AuthProtect "cookie-auth" :> "api" :> "event" :> Capture "event-id" (Key Event) :> "rsvp" :> "create.json" :> Post '[ JSON] AT.RsvpInfo
  , _rsvpDelete :: route :- AuthProtect "cookie-auth" :> "api" :> "event" :> Capture "event-id" (Key Event) :> "rsvp" :> "delete.json" :> Delete '[ JSON] ()
  , _static :: route :- Raw
  } deriving (Generic)

api :: Proxy (ToServantApi API)
api = genericApi (Proxy :: Proxy API)

server :: API (AsServerT AppM)
server =
  API
    { _index = indexEndpoint
    , _ping = pongEndpoint
    , _meetupsList = meetupsList
    , _meetupInfo = meetupInfo
    , _eventsList = eventsList
    , _accountInfo = accountInfoEndpoint
    , _logInSendPassword = logInSendPasswordEndpoint
    , _logInSendCode = logInSendCodeEndpoint
    , _updateAccount = updateAccount
    , _rsvpList = rsvpList
    , _rsvpCreate = rsvpCreate
    , _rsvpDelete = rsvpDelete
    , _static =
        serveDirectoryWith (WAS.defaultFileServerSettings "../../frontend/dist")
    }

-- | Redirects to https
httpApp :: Int -> Application
httpApp httpsPort req respond = respond response
  where
    mHostAndPort = Wai.requestHeaderHost req
    hostAndPort = fromMaybe defaultHost mHostAndPort
    host =
      case BC8.split ':' hostAndPort of
        (h:_) -> h
        _ -> defaultHost
    hostWithProtocol =
      "https://" <> host <> ":" <> S.fromString (show httpsPort)
    response =
      Wai.responseLBS HTTPStatus.status301 [("Location", hostWithProtocol)] ""

nt :: Env -> AppM a -> Servant.Handler a
nt s x = runReaderT x s

withPostgresqlPool ::
     P.ConnectionString
  -> Int
  -> (Pool P.SqlBackend -> RIO RioApp a)
  -> RIO RioApp a
withPostgresqlPool = P.withPostgresqlPool

genAuthServerContext ::
     Pool P.SqlBackend -> Context (AuthHandler Request (P.Entity User) ': '[])
genAuthServerContext db = (authHandler db) :. EmptyContext

main :: IO ()
main = do
  let psqlConnString =
        "host=localhost port=5432 user=postgres dbname=meetup password=password"
  let isVerbose = False
  logOptions' <- logOptionsHandle stderr isVerbose
  let logOptions = setLogUseTime True logOptions'
  withLogFunc logOptions $ \lf -> do
    let app = RioApp {appLogFunc = lf}
    runRIO app $ do
      args <- liftIO getArgs
      case args of
        ["generate-elm"] -> do
          liftIO $
            S.putStrLn $
            unlines
              ([ moduleHeader Elm0p18 "Meetup.Api"
               , ""
               , "import Json.Decode"
               , "import Json.Encode exposing (Value)"
               , "-- The following module comes from bartavelle/json-helpers"
               , "import Json.Helpers exposing (..)"
               , "import Dict exposing (Dict)"
               , "import Set exposing (Set)"
               , ""
               ] ++
               mkIntAliases ["Int64"] ++ [""]) ++
            makeModuleContent
              [ DefineElm (Proxy :: Proxy AT.JsonURI)
              , DefineElm (Proxy :: Proxy Condition)
              , DefineElm (Proxy :: Proxy AT.Meetup)
              , DefineElm (Proxy :: Proxy AT.Event)
              , DefineElm (Proxy :: Proxy AT.AccountInfo)
              , DefineElm (Proxy :: Proxy AT.LogInSendPasswordForm)
              , DefineElm (Proxy :: Proxy AT.LogInSendCodeForm)
              , DefineElm (Proxy :: Proxy AT.UpdateAccountForm)
              , DefineElm (Proxy :: Proxy AT.RsvpInfo)
              ]
        ["migrate"] -> do
          P.withPostgresqlPool psqlConnString 1 $ \pool -> do
            liftIO $ flip P.runSqlPool pool $ P.runMigration migrateAll
        ["drop-db-data"] -> do
          withPostgresqlPool psqlConnString 1 $ \pool -> dropDbData pool
        ["insert-stub-data"] -> do
          withPostgresqlPool psqlConnString 1 $ \pool -> insertStubData pool
        _ -> do
          logInfo "Starting app" :: RIO RioApp ()
          httpsPort <-
            return . fromMaybe 8081 . (>>= readMay) =<<
            liftIO (lookupEnv "MEETUP_HTTPS_PORT")
          httpPort <-
            return . fromMaybe 8080 . (>>= readMay) =<<
            liftIO (lookupEnv "MEETUP_HTTP_PORT")
          certPath <-
            return . fromMaybe "../../sysadmin/keys/certificate.pem" =<<
            liftIO (lookupEnv "MEETUP_CERT")
          keyPath <-
            return . fromMaybe "../../sysadmin/keys/key.pem" =<<
            liftIO (lookupEnv "MEETUP_KEY")
          mailgunDomain <-
            return . fromMaybe "meetup.vodka" =<<
            liftIO (lookupEnv "MEETUP_MAILGUN_DOMAIN")
          mailgunApiKey <-
            return . fromMaybe "" =<<
            liftIO (lookupEnv "MEETUP_MAILGUN_API_KEY")
          when (mailgunApiKey == "") $ do
            logError "Mailgun API key is empty!"
            error "Mailgun API key is empty!"
          P.withPostgresqlPool psqlConnString 5 $ \pool -> do
            let env =
                  Env
                    { envDb = pool
                    , envLogFunc = lf
                    , envConfig =
                        EnvConfig
                          { cfgMailgunDomain = S.toText mailgunDomain
                          , cfgMailgunApiKey = S.toText mailgunApiKey
                          }
                    }
            let tlsSettings = WarpTLS.tlsSettings certPath keyPath
            let settingsHttps = Warp.setPort httpsPort Warp.defaultSettings
            let runHttp :: RIO RioApp ()
                runHttp = liftIO (Warp.run httpPort (httpApp httpsPort))
            let runHttps :: RIO RioApp ()
                runHttps =
                  liftIO
                    (WarpTLS.runTLS
                       tlsSettings
                       settingsHttps
                       (serveWithContext
                          (Proxy :: Proxy (ToServantApi API))
                          (genAuthServerContext pool)
                          (hoistServerWithContext
                             (Proxy :: Proxy (ToServantApi API))
                             (Proxy :: Proxy '[ AuthHandler Request (P.Entity User)])
                             (nt env)
                             (genericServerT server))))
            withRunInIO $ \runInIO ->
              (Async.concurrently_ (runInIO runHttp) (runInIO runHttps))

mkIntAlias :: String -> [String]
mkIntAlias x =
  [ "type alias " <> x <> " = Int"
  , "jsonDec" <> x <> " = Json.Decode.int"
  , "jsonEnc" <> x <> " = Json.Encode.int"
  ]

mkIntAliases :: Foldable t => t String -> [String]
mkIntAliases xs = concatMap mkIntAlias xs
