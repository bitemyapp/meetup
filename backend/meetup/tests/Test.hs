module Main where

import RIO
import Test.Hspec
import Test.QuickCheck
import qualified Prelude

main :: IO ()
main =
  hspec $ do
    describe "Prelude.head" $ do
      it "returns the first element of a list" $ do
        Prelude.head [23 ..] `shouldBe` (23 :: Int)
      it "returns the first element of an *arbitrary* list" $
        property $ \x xs -> Prelude.head (x : xs) == (x :: Int)
      it "throws an exception if used with an empty list" $ do
        evaluate (Prelude.head []) `shouldThrow` anyException
