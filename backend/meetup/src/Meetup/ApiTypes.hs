{-# LANGUAGE TemplateHaskell #-}

module Meetup.ApiTypes where

import Elm.Derive
import Meetup.Aeson
import Network.URI (URI)
import RIO

data Meetup = Meetup
  { meeId :: Int64
  , meeTitle :: Text
  , meeDescription :: Text
  , meeOrganizerId :: Int64
  } deriving (Show, Eq, Generic)

data Event = Event
  { evtId :: Int64
  , evtDate :: Int
  } deriving (Show, Eq, Generic)

data AccountInfo = AccountInfo
  { accId :: Int64
  , accEmail :: Text
  , accFullName :: Maybe Text
  , accPhoneNumber :: Maybe Text
  , accInfoComplete :: Bool
  } deriving (Show, Eq, Generic)

data LogInSendPasswordForm = LogInSendPasswordForm
  { lisEmail :: Text
  } deriving (Show, Eq, Generic)

data LogInSendCodeForm = LogInSendCodeForm
  { licEmail :: Text
  , licCode :: Text
  } deriving (Show, Eq, Generic)

data UpdateAccountForm = UpdateAccountForm
  { uafFullName :: Text
  , uafPhoneNumber :: Text
  } deriving (Show, Eq, Generic)

data RsvpInfo = RsvpInfo
  { rsvName :: Text
  , rsvUserId :: Int64
  } deriving (Show, Eq, Generic)

-- | Don't use the constructor directly, convert from URI by using 'mkJsonURI'
newtype JsonURI =
  JsonURIDangerConstructor Text
  deriving (Show, Eq, Generic)

unJsonURI :: JsonURI -> Text
unJsonURI (JsonURIDangerConstructor x) = x

mkJsonURI :: URI -> JsonURI
mkJsonURI u = JsonURIDangerConstructor (tshow u)

deriveBoth (jsonOpts 0) ''JsonURI

deriveBoth (jsonOpts 3) ''Meetup

deriveBoth (jsonOpts 3) ''Event

deriveBoth (jsonOpts 3) ''AccountInfo

deriveBoth (jsonOpts 3) ''LogInSendPasswordForm

deriveBoth (jsonOpts 3) ''LogInSendCodeForm

deriveBoth (jsonOpts 3) ''UpdateAccountForm

deriveBoth (jsonOpts 3) ''RsvpInfo
