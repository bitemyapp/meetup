{-# LANGUAGE TemplateHaskell #-}

module Meetup.Types where

import Control.Newtype
import qualified Data.Aeson as J
import Data.Pool (Pool)
import qualified Data.String.Class as S
import Database.Persist
import qualified Database.Persist.Postgresql as P
import Database.Persist.Sql
import Elm.Derive
import Meetup.Aeson
import Meetup.Utils
import Network.URI (URI, parseAbsoluteURI)
import RIO
import Servant.API (FromHttpApiData, ToHttpApiData)
import Web.PathPieces (PathPiece)

data EnvConfig = EnvConfig
  { cfgMailgunDomain :: Text
  , cfgMailgunApiKey :: Text
  }

data Env = Env
  { envDb :: Pool P.SqlBackend
  , envLogFunc :: RIO.LogFunc
  , envConfig :: EnvConfig
  }

instance HasLogFunc Env where
  logFuncL = lens envLogFunc (\v a -> v {envLogFunc = a})

data Condition
  = New
  | LikeNew
  | VeryGood
  | Good
  | Acceptable
  | BetterThanNothing
  deriving (Eq, Show, Read, Generic)

deriveBoth (jsonOpts 0) ''Condition

instance PersistField Condition where
  toPersistValue x = toPersistValue (tshow x)
  fromPersistValue v = do
    t <- fromPersistValue v
    readEitherSafeText t

instance PersistFieldSql Condition where
  sqlType _ = SqlString

newtype LoginTokenVal =
  LoginTokenVal Text
  deriving ( Show
           , Eq
           , Read
           , PersistField
           , PersistFieldSql
           , Ord
           , PathPiece
           , ToHttpApiData
           , FromHttpApiData
           , J.ToJSON
           , J.FromJSON
           )

instance Newtype LoginTokenVal Text where
  pack = LoginTokenVal
  unpack (LoginTokenVal x) = x

newtype PersistentAbsoluteURI =
  PersistentAbsoluteURI URI
  deriving (Show, Eq)

newtype PhoneNumber =
  PhoneNumber Text
  deriving (Show, Eq, PersistFieldSql, PersistField)

instance Newtype PhoneNumber Text where
  pack = PhoneNumber
  unpack (PhoneNumber x) = x

newtype FullName =
  FullName Text
  deriving (Show, Eq, PersistFieldSql, PersistField)

instance Newtype FullName Text where
  pack = FullName
  unpack (FullName x) = x

instance Newtype PersistentAbsoluteURI URI where
  pack = PersistentAbsoluteURI
  unpack (PersistentAbsoluteURI x) = x

instance PersistField PersistentAbsoluteURI where
  toPersistValue x = toPersistValue (tshow (unpack x))
  fromPersistValue v = do
    t <- fromPersistValue v
    let mUri = parseAbsoluteURI (S.fromText t)
    maybe
      (fail ("Failed to parse an absolute URI: " <> (S.toString t)))
      (return . pack)
      mUri

instance PersistFieldSql PersistentAbsoluteURI where
  sqlType _ = SqlString
