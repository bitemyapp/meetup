module Meetup.Aeson where

import qualified Data.Aeson as J
import RIO

jsonOpts :: Int -> J.Options
jsonOpts n = J.defaultOptions {J.fieldLabelModifier = J.camelTo2 '_' . drop n}
