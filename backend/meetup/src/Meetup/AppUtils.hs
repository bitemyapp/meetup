module Meetup.AppUtils where

import Meetup.Types
import qualified Database.Persist.Postgresql as P
import RIO

runDb :: (MonadReader Env m, MonadIO m) => ReaderT P.SqlBackend IO b -> m b
runDb f = do
  pool <- asks envDb
  liftIO $ flip P.runSqlPool pool $ f
