module Meetup.Utils where

import qualified Data.String.Class as S
import qualified Data.Time.Clock as Time
import qualified Data.Time.Clock.POSIX as Time
import RIO
import Safe

readEitherSafeText :: Read a => Text -> Either Text a
readEitherSafeText t =
  case readEitherSafe (S.toString t) of
    Left s -> Left (S.toText s)
    Right r -> Right r

-- | Get the seconds in a 'NominalDiffTime'.
--
-- @since 1.9.1
nominalDiffTimeToSeconds :: Time.NominalDiffTime -> Int
nominalDiffTimeToSeconds t = floor (toRational t)

nominalDiffTimeToMilliseconds :: Time.NominalDiffTime -> Int
nominalDiffTimeToMilliseconds t = (nominalDiffTimeToSeconds t) * 1000

utcTimeToMilliseconds :: Time.UTCTime -> Int
utcTimeToMilliseconds =
  nominalDiffTimeToMilliseconds . Time.utcTimeToPOSIXSeconds
