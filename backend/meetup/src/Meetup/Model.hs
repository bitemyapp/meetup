{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE TemplateHaskell #-}

module Meetup.Model where

import Meetup.Types
import Data.Time.Clock (UTCTime)
import Database.Persist.Postgresql
import Database.Persist.TH
import RIO

share
  [mkPersist sqlSettings, mkMigrate "migrateAll"]
  [persistLowerCase|
User
    email Text
    phoneNumber PhoneNumber Maybe
    fullName FullName Maybe
    avatar_300 PersistentAbsoluteURI Maybe
    avatarOrig PersistentAbsoluteURI Maybe
    admin Bool
    createdAt UTCTime
    updatedAt UTCTime
    deriving Show
LoginToken
    tokenVal LoginTokenVal
    userId UserId
    createdAt UTCTime
    LoginTokenQuery tokenVal
    Primary tokenVal
    deriving Show
LoginCode
    code Text
    email Text
    createdAt UTCTime
    Primary code
    deriving Show
Meetup
    title Text
    description Text
    organizerId UserId
    deriving Show
Event
    meetupId MeetupId
    date UTCTime
    deriving Show
Rsvp
    userId UserId
    eventId EventId
    deriving Show
|]
