module Meetup.Queries where

import Data.Time.Clock (NominalDiffTime, addUTCTime, getCurrentTime)
import Database.Esqueleto
import Meetup.Model
import Meetup.Types
import RIO hiding ((^.), isNothing, on)

lookupUserByToken ::
     MonadIO m => LoginTokenVal -> ReaderT SqlBackend m (Maybe (Entity User))
lookupUserByToken tokenVal = do
  t <- liftIO getCurrentTime
  mToken <- getBy (LoginTokenQuery tokenVal)
  case mToken of
    Nothing -> return Nothing
    Just tok ->
      if addUTCTime tokenExpirationPeriod (loginTokenCreatedAt (entityVal tok)) >
         t
        then getEntity (loginTokenUserId (entityVal tok))
        else return Nothing

latestMeetups :: MonadIO m => ReaderT SqlBackend m [Entity Meetup]
latestMeetups = do
  latestMeetupIds <-
    select $
    from $ \event -> do
      orderBy [desc (event ^. EventDate)]
      limit 100
      return $ event ^. EventMeetupId
  meetups <-
    select $
    from $ \meetup -> do
      where_ $ meetup ^. MeetupId `in_` valList (map unValue latestMeetupIds)
      return meetup
  return meetups

-- TODO: move out in Constants.hs
tokenExpirationPeriod :: NominalDiffTime
tokenExpirationPeriod = 60 * 60 * 24 * 10

loginCodeExpirationPeriod :: NominalDiffTime
loginCodeExpirationPeriod = 60 * 10
