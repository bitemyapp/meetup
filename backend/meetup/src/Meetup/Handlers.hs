{-# LANGUAGE QuasiQuotes #-}

module Meetup.Handlers where

import Control.Monad.Logger (MonadLogger(..), toLogStr)
import Control.Newtype
import qualified Data.Aeson as J
import qualified Data.Char as Char
import qualified Data.Map.Strict as Map
import Data.Pool (Pool)
import qualified Data.String.Class as S
import Data.String.Class (toText)
import Data.Time.Clock (addUTCTime, getCurrentTime)
import qualified Data.UUID as UUID
import qualified Data.UUID.V4 as UUID4
import qualified Database.Persist.Postgresql as P
import qualified Mail.Hailgun as Hailgun
import qualified Meetup.ApiTypes as AT
import Meetup.AppUtils
import Meetup.Model
import qualified Meetup.Queries as Q
import Meetup.Types
import Meetup.Utils
import Network.Wai (Request, requestHeaders)
import RIO
import Servant
import Servant.Server.Experimental.Auth
  ( AuthHandler
  , AuthServerData
  , mkAuthHandler
  )
import System.Log.FastLogger (fromLogStr)
import qualified Test.RandomStrings as RandomStrings
import Text.InterpolatedString.Perl6 (qc)
import Web.Cookie
  ( SetCookie
  , defaultSetCookie
  , parseCookies
  , setCookieName
  , setCookieValue
  )

type AppM = ReaderT Env Servant.Handler

data RioApp = RioApp
  { appLogFunc :: LogFunc
  }

instance MonadLogger (RIO RioApp) where
  monadLoggerLog _loc _logSource _logLevel msg =
    liftIO (S.putStrLn (fromLogStr (toLogStr msg)))

instance HasLogFunc RioApp where
  logFuncL = lens appLogFunc (\x y -> x {appLogFunc = y})

-- | We need to specify the data returned after authentication
type instance AuthServerData (AuthProtect "cookie-auth") =
     P.Entity User

indexEndpoint :: AppM Text
indexEndpoint = throwError $ err302 {errHeaders = [("Location", "/index.html")]}

pongEndpoint :: AppM Text
pongEndpoint = return "pong"

meetupsList :: AppM [AT.Meetup]
meetupsList = do
  meetups <- runDb $ Q.latestMeetups
  return (map convMeetup meetups)

convMeetup :: P.Entity Meetup -> AT.Meetup
convMeetup meetup =
  let ev = P.entityVal meetup
   in AT.Meetup
        { AT.meeId = P.fromSqlKey (P.entityKey meetup)
        , AT.meeTitle = meetupTitle ev
        , AT.meeDescription = meetupDescription ev
        , AT.meeOrganizerId = P.fromSqlKey (meetupOrganizerId ev)
        }

meetupInfo :: Key Meetup -> AppM AT.Meetup
meetupInfo meetupId = do
  meetup <- mustFindM $ runDb $ P.getEntity meetupId
  return $ convMeetup meetup

eventsList :: Key Meetup -> AppM [AT.Event]
eventsList meetupId = do
  events <- runDb $ P.selectList [EventMeetupId P.==. meetupId] []
  return (map conv events)
  where
    conv event =
      let ev = P.entityVal event
       in AT.Event
            { evtId = P.fromSqlKey (P.entityKey event)
            , evtDate = utcTimeToMilliseconds (eventDate ev)
            }

accountInfoEndpoint :: P.Entity User -> AppM AT.AccountInfo
accountInfoEndpoint userEntity = do
  let user = P.entityVal userEntity
  return $
    AT.AccountInfo
      { accId = P.fromSqlKey (P.entityKey userEntity)
      , accEmail = userEmail user
      , accInfoComplete = isInfoComplete user
      , accFullName = unpack <$> userFullName user
      , accPhoneNumber = unpack <$> userPhoneNumber user
      }

logInSendPasswordEndpoint :: AT.LogInSendPasswordForm -> AppM ()
logInSendPasswordEndpoint AT.LogInSendPasswordForm {..} = do
  env <- ask
  code <-
    liftIO $
    (S.toText . map Char.toUpper) <$>
    RandomStrings.randomString
      (RandomStrings.onlyAlphaNum RandomStrings.randomASCII)
      5
  logInfo $
    "Generated code for email: " <> display lisEmail <> "; code: " <>
    display code
  t <- liftIO $ getCurrentTime
  _ <-
    runDb $
    P.insert
      LoginCode
        { loginCodeCode = code
        , loginCodeCreatedAt = t
        , loginCodeEmail = lisEmail
        }
  let msgTxt =
        [qc|
          Meowdy, partner

Your sign-in code is: {code}

All the best
          |]
      msgHtml =
        [qc|Meowdy, partner<br><br>

Your sign-in code is: {code}<br><br>

All the best
          |]
  let ctx =
        Hailgun.HailgunContext
          { hailgunDomain = S.toString (cfgMailgunDomain (envConfig env))
          , hailgunApiKey = S.toString (cfgMailgunApiKey (envConfig env))
          , hailgunProxy = Nothing
          }
  pure
    (Hailgun.hailgunMessage
       "Meetup Sign-In Link"
       (Hailgun.TextAndHTML msgTxt msgHtml)
       "meetup.vodka@gmail.com"
       (Hailgun.MessageRecipients
          { recipientsTo = [S.fromText lisEmail]
          , recipientsCC = []
          , recipientsBCC = []
          })
       []) >>= \case
    Left err -> do
      let e = "Error while constructing Hailgun email: " <> (fromString err)
      logError e
      throwError $ err500 {errBody = S.toLazyByteString (utf8BuilderToText e)}
    Right msg -> do
      liftIO (Hailgun.sendEmail ctx msg) >>= \case
        Left err -> do
          let e =
                "Error while sending Hailgun email: " <>
                display (S.toText (Hailgun.herMessage err))
          logError e
          throwError $
            err500 {errBody = S.toLazyByteString (utf8BuilderToText e)}
        Right sendRsp -> do
          logInfo $
            "Email sent. Mailgun response: " <>
            fromString (Hailgun.hsrMessage sendRsp)
  return ()

logInSendCodeEndpoint ::
     AT.LogInSendCodeForm
  -> AppM (Headers '[ Header "Set-Cookie" SetCookie] AT.AccountInfo)
logInSendCodeEndpoint AT.LogInSendCodeForm {..} = do
  mLoginCode <-
    runDb $
    P.selectFirst
      [LoginCodeCode P.==. licCode, LoginCodeEmail P.==. licEmail]
      []
  case mLoginCode of
    Nothing -> noHeader <$> formErrors [("code", badCode)]
    Just loginCode -> do
      t <- liftIO getCurrentTime
      if addUTCTime
           Q.loginCodeExpirationPeriod
           (loginCodeCreatedAt (P.entityVal loginCode)) >
         t
        then do
          tval <- liftIO $ UUID.toText <$> UUID4.nextRandom
          user <-
            runDb $ do
              mUser <- P.selectFirst [UserEmail P.==. licEmail] []
              user <-
                case mUser of
                  Nothing -> do
                    userKey <-
                      P.insert
                        User
                          { userEmail = licEmail
                          , userPhoneNumber = Nothing
                          , userFullName = Nothing
                          , userAvatar_300 = Nothing
                          , userAvatarOrig = Nothing
                          , userAdmin = False
                          , userCreatedAt = t
                          , userUpdatedAt = t
                          }
                    P.getJustEntity userKey
                  Just user -> pure user
              _ <-
                P.insert
                  LoginToken
                    { loginTokenTokenVal = pack (tval)
                    , loginTokenUserId = P.entityKey user
                    , loginTokenCreatedAt = t
                    }
              pure user
          let cookie =
                defaultSetCookie
                  {setCookieName = "u", setCookieValue = S.fromText tval}
          let accInfo =
                AT.AccountInfo
                  { accId = P.fromSqlKey (P.entityKey user)
                  , accEmail = licEmail
                  , accInfoComplete = isInfoComplete (P.entityVal user)
                  , accFullName = unpack <$> userFullName (P.entityVal user)
                  , accPhoneNumber =
                      unpack <$> userPhoneNumber (P.entityVal user)
                  }
          return $ addHeader cookie accInfo
        else noHeader <$> formErrors [("code", badCode)]
  where
    badCode = "Wrong sign-in code"

updateAccount :: P.Entity User -> AT.UpdateAccountForm -> AppM AT.AccountInfo
updateAccount user AT.UpdateAccountForm {..} = do
  user2 <-
    runDb $
    P.updateGet
      (P.entityKey user)
      [ UserFullName P.=. Just (pack uafFullName)
      , UserPhoneNumber P.=. Just (pack uafPhoneNumber)
      ]
  accountInfoEndpoint (P.Entity (P.entityKey user) user2)

rsvpList :: Key Event -> AppM [AT.RsvpInfo]
rsvpList eventId = do
  _event <- mustFindM $ runDb $ P.getEntity eventId
  rsvpEnts <- runDb $ P.selectList [RsvpEventId P.==. eventId] []
  let rsvps = map P.entityVal rsvpEnts
  let userKeys = map rsvpUserId rsvps
  usersMap <- runDb $ P.getMany userKeys
  return $ mapMaybe (conv usersMap) rsvps
  where
    conv usersMap rsvp = do
      user <- Map.lookup (rsvpUserId rsvp) usersMap
      Just
        (AT.RsvpInfo
           { rsvName = fromMaybe "" (unpack <$> (userFullName user))
           , rsvUserId = P.fromSqlKey (rsvpUserId rsvp)
           })

rsvpCreate :: P.Entity User -> Key Event -> AppM AT.RsvpInfo
rsvpCreate user eventId = do
  event <- mustFindM $ runDb $ P.getEntity eventId
  _ <-
    runDb $
    P.insert $
    Rsvp {rsvpUserId = P.entityKey user, rsvpEventId = P.entityKey event}
  return $
    AT.RsvpInfo
      { rsvName = fromMaybe "" (unpack <$> userFullName (P.entityVal user))
      , rsvUserId = P.fromSqlKey (P.entityKey user)
      }

rsvpDelete :: P.Entity User -> Key Event -> AppM ()
rsvpDelete user eventId = do
  runDb $
    P.deleteWhere [RsvpEventId P.==. eventId, RsvpUserId P.==. P.entityKey user]
  return ()

formErrors :: [(Text, Text)] -> AppM a
formErrors xs = throwError err400 {errBody = J.encode xs}

lookupAccount ::
     Pool P.SqlBackend -> LoginTokenVal -> Servant.Handler (P.Entity User)
lookupAccount db tok = do
  mToken <- liftIO $ flip P.runSqlPool db $ Q.lookupUserByToken tok
  case mToken of
    Nothing -> throwError (err403 {errBody = "Invalid Token"})
    Just user -> return user

authHandler :: Pool P.SqlBackend -> AuthHandler Request (P.Entity User)
authHandler db = mkAuthHandler handler
  where
    maybeToEither e = maybe (Left e) Right
    throw401 msg = throwError $ err401 {errBody = msg}
    handler req =
      either throw401 (lookupAccount db . pack . toText) $ do
        cookie <-
          maybeToEither "Missing cookie header" $
          lookup "cookie" $ requestHeaders req
        maybeToEither "Missing token in cookie" $
          lookup "u" $ parseCookies cookie

isInfoComplete :: User -> Bool
isInfoComplete user =
  if isEmpty (fmap (unpack :: FullName -> Text) (userFullName user)) ||
     isEmpty (fmap (unpack :: PhoneNumber -> Text) (userPhoneNumber user))
    then False
    else True
  where
    isEmpty Nothing = True
    isEmpty (Just "") = True
    isEmpty _ = False

mustFind :: Maybe a -> AppM a
mustFind = maybe notFound pure

mustFindM :: AppM (Maybe a) -> AppM a
mustFindM = (=<<) mustFind

notFound :: AppM a
notFound = throwError err404
