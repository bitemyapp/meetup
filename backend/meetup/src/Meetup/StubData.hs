module Meetup.StubData where

import Control.Newtype
import Data.Pool (Pool)
import Data.Time.Clock (getCurrentTime)
import qualified Database.Persist.Postgresql as P
import Meetup.Handlers
import Meetup.Model
import Prelude
import RIO

dropDbData :: Pool P.SqlBackend -> RIO RioApp ()
dropDbData pool = do
  flip P.runSqlPool pool $ do
    P.deleteWhere ([] :: [P.Filter Event])
    P.deleteWhere ([] :: [P.Filter Meetup])
    P.deleteWhere ([] :: [P.Filter User])
    P.deleteWhere ([] :: [P.Filter Rsvp])

insertStubData :: Pool P.SqlBackend -> RIO RioApp ()
insertStubData pool = do
  flip P.runSqlPool pool $ do
    t <- liftIO getCurrentTime
    let meetupUser =
          User
            { userEmail = "k-bx@k-bx.com"
            , userPhoneNumber = Just $ pack "+380731787269"
            , userFullName = Just $ pack "Meetup"
            , userAvatar_300 = Nothing
            , userAvatarOrig = Nothing
            , userAdmin = True
            , userCreatedAt = t
            , userUpdatedAt = t
            }
    userId <- P.insert meetupUser
    let meetup01 =
          Meetup
            { meetupTitle = "Elm Study Group"
            , meetupDescription =
                "Join us at [Elm Study Group](https://github.com/KyivHaskell/elm-study-group) meetup"
            , meetupOrganizerId = userId
            }
    meetupId01 <- P.insert meetup01
    let event01 =
          Event
            { eventMeetupId = meetupId01
            , eventDate = Prelude.read "2019-02-28 21:00:00"
            }
    _eventId01 <- P.insert event01
    return ()
