pre:
	rm -rf ./dist
	mkdir -p ./dist/frontend
	mkdir -p ./dist/backend/meetup
	mkdir -p ./dist/sysadmin
.PHONY: pre

post:
	cd frontend && cp -r ./dist ../dist/frontend/
	cd backend/meetup && cp $$(stack exec -- which meetup) ../../dist/backend/meetup/
	cp -r ./sysadmin/keys ./dist/sysadmin/
.PHONY: post

prod:
	make pre
	make generate-elm
	cd frontend && make prod
	cd backend && make prod
	make post
.PHONY: prod

fast-fe:
	make pre
	make generate-elm
	cd frontend && make build
	make post
.PHONY: fast

fast-be:
	make pre
	cd backend && make fast
	make post
.PHONY: fast-be

run-be:
	cd backend/meetup && $$(stack exec -- which meetup)
.PHONY: run-be

run-dev:
	# sudo apt install inotify-tools
	./run-dev.sh
.PHONY: run-dev

generate-elm:
	cd backend/meetup && stack run -- generate-elm > ../../frontend/src/Meetup/Api.elm
.PHONY: generate-elm

deploy:
	ssh ubuntu@${MEETUP_HOST} rm -rf /home/ubuntu/meetup
	ssh ubuntu@${MEETUP_HOST} mkdir -p /home/ubuntu/meetup
	rsync -vrzah dist/ ubuntu@${MEETUP_HOST}:/home/ubuntu/meetup
	ssh ubuntu@${MEETUP_HOST} sudo systemctl restart meetup
.PHONY: deploy

sysadmin-setup:
	ssh ubuntu@meetup sudo apt-get install -y libpq-dev
	scp sysadmin/meetup.service root@meetup:/etc/systemd/system/meetup.service
	ssh ubuntu@meetup sudo systemctl daemon-reload
	# decided to not include the postgresql setup, because it should
	# be moved out eventually anyways
.PHONY: sysadmin-setup

migrate-prod:
	ssh -t ubuntu@meetup "cd /home/ubuntu/meetup/backend/meetup && ./meetup migrate"

psql-prod:
	ssh -t ubuntu@meetup "psql -h localhost -U postgres -W meetup"

tags:
	hasktags -e .
	mv TAGS TAGS01
	find . -type f -name "*.elm" -print | etags --language=none --regex=@elm.tags -
	mv TAGS TAGS02
	cat TAGS02 >> TAGS
	rm TAGS02
	cat TAGS01 >> TAGS
	rm TAGS01
.PHONY: tags
