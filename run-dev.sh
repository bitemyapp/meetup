#!/bin/sh
while true; do
    if make fast-be && make fast-fe ; then
        (cd dist/backend/meetup && ./meetup) &
    fi
    PID=$!
    echo ">> PID: $PID"
    inotifywait \
        -r \
        -e modify \
        -e attrib \
        -e move \
        -e create \
        -e delete \
        --exclude '.*(#|\.git).*' \
        .
    kill $PID
    echo "================================================"
done

# TODO: rewrite via https://github.com/haskell-fswatch/hfsnotify
