# Meetup.Vodka

https://meetup.vodka

Meetup.vodka: open-source project where you can register your meetup, giving ability to RSVP to your guests. Serves as a showcase real-world app for Haskell + Elm.

Develop with:

```
make run-dev
```
