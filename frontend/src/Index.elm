module Main exposing (main)

import Bootstrap.Alert as Alert
import Bootstrap.Button as Button
import Bootstrap.Card as Card
import Bootstrap.Card.Block as Block
import Bootstrap.Grid as Grid
import Bootstrap.Grid.Col as Col
import Bootstrap.ListGroup as Listgroup
import Bootstrap.Modal as Modal
import Bootstrap.Navbar as Navbar
import Bootstrap.Text as Text
import Browser exposing (UrlRequest)
import Browser.Navigation as Navigation
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (onClick)
import Http
import Json.Encode as Json
import Markdown
import Meetup.Api as Api
import Meetup.Lib exposing (handleSubpage, updateMsgs)
import Meetup.Model exposing (..)
import Meetup.Pages.Account as AccountPage
import Meetup.Pages.Account.Model as AccountPage
import Meetup.Pages.Catalog as CatalogPage
import Meetup.Pages.Catalog.Model as CatalogPage
import Meetup.Pages.Home as HomePage
import Meetup.Pages.Login as LoginPage
import Meetup.Pages.Login.Model as LoginPage
import Meetup.Pages.Meetup as MeetupPage
import Meetup.Pages.Meetup.Model as MeetupPage
import Meetup.Util exposing (..)
import String
import Url exposing (Url)
import Url.Builder as UrlBuilder
import Url.Parser as UrlParser exposing ((</>), Parser, int, s, top)


main : Program Flags Model Msg
main =
    Browser.application
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        , onUrlRequest = ClickedLink
        , onUrlChange = UrlChange
        }


init : Flags -> Url -> Navigation.Key -> ( Model, Cmd Msg )
init flags url key =
    let
        ( navState, navCmd ) =
            Navbar.initialState NavMsg

        ( model, urlCmd ) =
            urlUpdate url
                { navKey = key
                , navState = navState
                , page = PageHome
                , pageModel = HomePageModel ()
                , redirectOnLogin = Nothing
                }
    in
    ( model, Cmd.batch [ urlCmd, navCmd ] )


subscriptions : Model -> Sub Msg
subscriptions model =
    Navbar.subscriptions model.navState NavMsg


needLogin model backPage =
    ( { model
        | page = PageLogin
        , pageModel = LoginPageModel LoginPage.init
        , redirectOnLogin = Just backPage
      }
    , Cmd.map LoginPageMsg <| LoginPage.loadAccountInfo
    )


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case ( model.pageModel, msg ) of
        ( _, ClickedLink req ) ->
            case req of
                Browser.Internal url ->
                    ( model
                    , Navigation.pushUrl model.navKey <| Url.toString url
                    )

                Browser.External href ->
                    ( model, Navigation.load href )

        ( _, UrlChange url ) ->
            urlUpdate url model

        ( _, NavMsg state ) ->
            ( { model | navState = state }
            , Cmd.none
            )

        ( CatalogPageModel pageModel, CatalogPageMsg m ) ->
            handleSubpage
                update
                m
                pageModel
                (\pm -> { model | pageModel = CatalogPageModel pm })
                CatalogPage.update
                CatalogPageMsg

        ( MeetupPageModel pageModel, MeetupPageMsg MeetupPage.NeedLogin ) ->
            needLogin model (PageMeetup pageModel.meetupId)

        ( MeetupPageModel pageModel, MeetupPageMsg m ) ->
            handleSubpage
                update
                m
                pageModel
                (\pm -> { model | pageModel = MeetupPageModel pm })
                MeetupPage.update
                MeetupPageMsg

        ( LoginPageModel pageModel, LoginPageMsg ((LoginPage.CodeSent (Ok accInfo)) as m) ) ->
            if accInfo.info_complete then
                redirectBack model

            else
                let
                    ( newPageModel, pageMsgs ) =
                        LoginPage.update m pageModel
                in
                ( { model | pageModel = LoginPageModel newPageModel }
                , Cmd.map LoginPageMsg pageMsgs
                )

        ( _, LoginPageMsg (LoginPage.UpdateAccountDone (Ok _)) ) ->
            redirectBack model

        ( LoginPageModel pageModel, LoginPageMsg m ) ->
            let
                ( newPageModel, pageMsgs ) =
                    LoginPage.update m pageModel
            in
            ( { model | pageModel = LoginPageModel newPageModel }
            , Cmd.map LoginPageMsg pageMsgs
            )

        ( _, AccountPageMsg AccountPage.NeedLogin ) ->
            needLogin model PageAccount

        ( AccountPageModel pageModel, AccountPageMsg m ) ->
            handleSubpage
                update
                m
                pageModel
                (\pm -> { model | pageModel = AccountPageModel pm })
                AccountPage.update
                AccountPageMsg

        _ ->
            ( model, Cmd.none )


urlUpdate : Url -> Model -> ( Model, Cmd Msg )
urlUpdate url model =
    case decode url of
        Nothing ->
            ( { model | page = PageNotFound }, Cmd.none )

        Just route ->
            let
                ( m, cmds ) =
                    initPageModel route
            in
            ( { model | page = route, pageModel = m }, cmds )


decode : Url -> Maybe Page
decode url =
    { url | path = Maybe.withDefault "" url.fragment, fragment = Nothing }
        |> UrlParser.parse routeParser


redirectBack model =
    let
        p =
            case model.redirectOnLogin of
                Just page ->
                    page

                Nothing ->
                    PageAccount

        ( m, cmds ) =
            initPageModel p
    in
    ( { model | page = p, pageModel = m, redirectOnLogin = Nothing }, cmds )


routeParser : Parser (Page -> a) a
routeParser =
    UrlParser.oneOf
        [ UrlParser.map PageHome top
        , UrlParser.map PageCatalog (s "meetups")
        , UrlParser.map PageMeetup (s "meetup" </> int)
        , UrlParser.map PageAccount (s "account")
        ]


view : Model -> Browser.Document Msg
view model =
    { title = "Meetup.Vodka"
    , body =
        [ div []
            [ header
            , menu model
            , mainContent model
            ]
        ]
    }


header : Html Msg
header =
    div []
        [ -- img
          --    [ alt "Meetup.Vodka"
          --    , class "rounded mx-auto d-block"
          --    , height 100
          --    , width 100
          --    ]
          --    []
          div [ class "container mb-4 mt-4 heading" ]
            [ text "Meetup.Vodka" ]
        ]


menu : Model -> Html Msg
menu model =
    Navbar.config NavMsg
        |> Navbar.withAnimation
        |> Navbar.container
        |> Navbar.brand [ href "#" ] [ text "Main" ]
        |> Navbar.items
            [ Navbar.itemLink [ href "#meetups" ] [ text "Meetups" ]
            , Navbar.itemLink [ href "#account" ] [ text "Account" ]
            ]
        |> Navbar.view model.navState


mainContent : Model -> Html Msg
mainContent model =
    Grid.container [] <|
        case ( model.page, model.pageModel ) of
            ( PageHome, HomePageModel _ ) ->
                HomePage.view

            ( PageCatalog, CatalogPageModel pm ) ->
                (List.map << Html.map) CatalogPageMsg <| CatalogPage.view pm

            ( PageMeetup _, MeetupPageModel pm ) ->
                (List.map << Html.map) MeetupPageMsg <| MeetupPage.view pm

            ( PageAccount, AccountPageModel pm ) ->
                (List.map << Html.map) AccountPageMsg <| AccountPage.view pm

            ( PageLogin, LoginPageModel pm ) ->
                (List.map << Html.map) LoginPageMsg <| LoginPage.view pm

            ( PageNotFound, _ ) ->
                pageNotFound

            _ ->
                pageNotFound


pageNotFound : List (Html Msg)
pageNotFound =
    [ h1 [] [ text "Not found" ]
    , text "Sorry couldn't find that page"
    ]
