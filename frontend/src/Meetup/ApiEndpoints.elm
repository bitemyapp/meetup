module Meetup.ApiEndpoints exposing
    ( loadAccountInfo
    , loadCatalog
    , loadMeetup
    , loadMeetupEvents
    , logInSendPassword
    , login
    , rsvpCreate
    , rsvpDelete
    , rsvpList
    , updateAccount
    )

import Http
import Json.Decode as J
import Json.Encode as Encode
import Meetup.Api as Api
import Url.Builder as UrlBuilder


loadCatalog : (Result Http.Error (List Api.Meetup) -> msg) -> Cmd msg
loadCatalog toMsg =
    Http.send
        toMsg
        (Http.get (UrlBuilder.absolute [ "api", "meetups.json" ] [])
            (J.list Api.jsonDecMeetup)
        )


loadMeetup : (Result Http.Error Api.Meetup -> msg) -> Int -> Cmd msg
loadMeetup toMsg meetupId =
    Http.send toMsg
        (Http.get
            (UrlBuilder.absolute
                [ "api", "meetup", String.fromInt meetupId, "info.json" ]
                []
            )
            Api.jsonDecMeetup
        )


loadMeetupEvents : (Result Http.Error (List Api.Event) -> msg) -> Int -> Cmd msg
loadMeetupEvents toMsg meetupId =
    Http.send toMsg
        (Http.get
            (UrlBuilder.absolute
                [ "api", "meetup", String.fromInt meetupId, "events.json" ]
                []
            )
            (J.list Api.jsonDecEvent)
        )


loadAccountInfo : (Result Http.Error Api.AccountInfo -> msg) -> Cmd msg
loadAccountInfo toMsg =
    Http.send toMsg
        (Http.get
            (UrlBuilder.absolute [ "api", "account-info.json" ] [])
            Api.jsonDecAccountInfo
        )


logInSendPassword :
    (Result Http.Error () -> msg)
    -> Api.LogInSendPasswordForm
    -> Cmd msg
logInSendPassword toMsg form =
    Http.send toMsg
        (Http.post
            (UrlBuilder.absolute [ "api", "log-in-send-password" ] [])
            (Http.jsonBody (Api.jsonEncLogInSendPasswordForm form))
            (J.succeed ())
        )


login :
    (Result Http.Error Api.AccountInfo -> msg)
    -> Api.LogInSendCodeForm
    -> Cmd msg
login toMsg form =
    Http.send toMsg
        (Http.post
            (UrlBuilder.absolute [ "api", "log-in" ] [])
            (Http.jsonBody (Api.jsonEncLogInSendCodeForm form))
            Api.jsonDecAccountInfo
        )


updateAccount :
    (Result Http.Error Api.AccountInfo -> msg)
    -> Api.UpdateAccountForm
    -> Cmd msg
updateAccount toMsg form =
    Http.send toMsg
        (Http.post
            (UrlBuilder.absolute [ "api", "account", "update.json" ] [])
            (Http.jsonBody (Api.jsonEncUpdateAccountForm form))
            Api.jsonDecAccountInfo
        )


rsvpList :
    (Result Http.Error (List Api.RsvpInfo) -> msg)
    -> Int
    -> Cmd msg
rsvpList toMsg eventId =
    Http.send toMsg
        (Http.get
            (UrlBuilder.absolute [ "api", "event", String.fromInt eventId, "rsvp", "list.json" ] [])
            (J.list Api.jsonDecRsvpInfo)
        )


rsvpCreate :
    (Result Http.Error Api.RsvpInfo -> msg)
    -> Int
    -> Cmd msg
rsvpCreate toMsg eventId =
    Http.send toMsg
        (Http.post
            (UrlBuilder.absolute [ "api", "event", String.fromInt eventId, "rsvp", "create.json" ] [])
            (Http.jsonBody (Encode.string ""))
            Api.jsonDecRsvpInfo
        )


rsvpDelete :
    (Result Http.Error () -> msg)
    -> Int
    -> Cmd msg
rsvpDelete toMsg eventId =
    Http.send toMsg
        (Http.request
            { method = "DELETE"
            , headers = []
            , url = UrlBuilder.absolute [ "api", "event", String.fromInt eventId, "rsvp", "delete.json" ] []
            , body = Http.jsonBody (Encode.string "")
            , expect = Http.expectStringResponse (always (Ok ()))
            , timeout = Nothing
            , withCredentials = False
            }
        )
