module Meetup.Util exposing
    ( either
    , fromMaybe
    , keepOks
    , mapOk
    , maybe
    , showCondition
    , showHttpError
    , splitByN
    )

import Meetup.Api as Api
import Http


showHttpError : Http.Error -> String
showHttpError e =
    case e of
        Http.BadUrl s ->
            "Bad url: " ++ s

        Http.Timeout ->
            "Timeout"

        Http.NetworkError ->
            "Network error"

        Http.BadStatus _ ->
            "Bad status"

        Http.BadPayload msg _ ->
            "Bad payload"


showCondition : Api.Condition -> String
showCondition x =
    case x of
        Api.New ->
            "New"

        Api.LikeNew ->
            "Like new"

        Api.VeryGood ->
            "Very Good"

        Api.Good ->
            "Good"

        Api.Acceptable ->
            "Acceptable"

        Api.BetterThanNothing ->
            "Better Than Nothing"


splitByN : Int -> List a -> List (List a)
splitByN i list =
    case List.take i list of
        [] ->
            []

        listHead ->
            listHead :: splitByN i (List.drop i list)


keepOks : List (Result a b) -> List b
keepOks xss =
    case xss of
        [] ->
            []

        x :: xs ->
            case x of
                Err _ ->
                    keepOks xs

                Ok v ->
                    v :: keepOks xs


mapOk : (b -> c) -> Result a b -> Result a c
mapOk f x =
    case x of
        Err e ->
            Err e

        Ok v ->
            Ok (f v)


either : (a -> c) -> (b -> d) -> Result a b -> Result c d
either fa fb res =
    case res of
        Err a ->
            Err (fa a)

        Ok b ->
            Ok (fb b)


fromMaybe : a -> Maybe a -> a
fromMaybe =
    Maybe.withDefault


maybe : a -> (b -> a) -> Maybe b -> a
maybe a f mb =
    case mb of
        Nothing ->
            a

        Just b ->
            f b
