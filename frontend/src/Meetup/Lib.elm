module Meetup.Lib exposing
    ( fromFormErrors
    , handleHttpError
    , handleSubpage
    , updateMsgs
    )

import Dict exposing (Dict)
import Http


handleHttpError e m needLoginMsg =
    case e of
        Http.BadStatus rsp ->
            case rsp.status.code of
                401 ->
                    ( m, [ needLoginMsg ], Cmd.none )

                403 ->
                    ( m, [ needLoginMsg ], Cmd.none )

                _ ->
                    ( { m
                        | errors = [ "There was an error, please try again later" ]
                      }
                    , []
                    , Cmd.none
                    )

        _ ->
            ( { m
                | errors = [ "There was an error, please try again later" ]
              }
            , []
            , Cmd.none
            )


updateMsgs :
    (msg -> model -> ( model, Cmd msg ))
    -> List msg
    -> model
    -> ( model, Cmd msg )
updateMsgs update msgs model =
    case msgs of
        [] ->
            ( model, Cmd.none )

        x :: xs ->
            let
                ( model2, cmd ) =
                    update x model

                ( model3, cmd2 ) =
                    updateMsgs update xs model2
            in
            ( model3, Cmd.batch [ cmd, cmd2 ] )


handleSubpage update m pageModel setPageModel pageUpdate toMsg =
    let
        ( newPageModel, pageMsgs, pageCmd ) =
            pageUpdate m pageModel

        newModel =
            setPageModel newPageModel

        newMsgs =
            List.map toMsg pageMsgs

        newCmd =
            Cmd.map toMsg pageCmd

        ( newModel2, newCmds2 ) =
            updateMsgs update newMsgs newModel
    in
    ( newModel2, Cmd.batch [ newCmd, newCmds2 ] )


fromFormErrors : Dict String String -> String -> Result String a -> Result String a
fromFormErrors formErrors field val =
    case Dict.get field formErrors of
        Nothing ->
            val

        Just err ->
            Err err
