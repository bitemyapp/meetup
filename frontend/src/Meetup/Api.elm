module Meetup.Api exposing(..)

import Json.Decode
import Json.Encode exposing (Value)
-- The following module comes from bartavelle/json-helpers
import Json.Helpers exposing (..)
import Dict exposing (Dict)
import Set exposing (Set)

type alias Int64 = Int
jsonDecInt64 = Json.Decode.int
jsonEncInt64 = Json.Encode.int

type alias JsonURI  = String

jsonDecJsonURI : Json.Decode.Decoder ( JsonURI )
jsonDecJsonURI =
    Json.Decode.string

jsonEncJsonURI : JsonURI -> Value
jsonEncJsonURI  val = Json.Encode.string val



type Condition  =
    New 
    | LikeNew 
    | VeryGood 
    | Good 
    | Acceptable 
    | BetterThanNothing 

jsonDecCondition : Json.Decode.Decoder ( Condition )
jsonDecCondition = 
    let jsonDecDictCondition = Dict.fromList [("New", New), ("LikeNew", LikeNew), ("VeryGood", VeryGood), ("Good", Good), ("Acceptable", Acceptable), ("BetterThanNothing", BetterThanNothing)]
    in  decodeSumUnaries "Condition" jsonDecDictCondition

jsonEncCondition : Condition -> Value
jsonEncCondition  val =
    case val of
        New -> Json.Encode.string "New"
        LikeNew -> Json.Encode.string "LikeNew"
        VeryGood -> Json.Encode.string "VeryGood"
        Good -> Json.Encode.string "Good"
        Acceptable -> Json.Encode.string "Acceptable"
        BetterThanNothing -> Json.Encode.string "BetterThanNothing"



type alias Meetup  =
   { id: Int64
   , title: String
   , description: String
   , organizer_id: Int64
   }

jsonDecMeetup : Json.Decode.Decoder ( Meetup )
jsonDecMeetup =
   Json.Decode.succeed (\pid ptitle pdescription porganizer_id -> {id = pid, title = ptitle, description = pdescription, organizer_id = porganizer_id})
   |> required "id" (jsonDecInt64)
   |> required "title" (Json.Decode.string)
   |> required "description" (Json.Decode.string)
   |> required "organizer_id" (jsonDecInt64)

jsonEncMeetup : Meetup -> Value
jsonEncMeetup  val =
   Json.Encode.object
   [ ("id", jsonEncInt64 val.id)
   , ("title", Json.Encode.string val.title)
   , ("description", Json.Encode.string val.description)
   , ("organizer_id", jsonEncInt64 val.organizer_id)
   ]



type alias Event  =
   { id: Int64
   , date: Int
   }

jsonDecEvent : Json.Decode.Decoder ( Event )
jsonDecEvent =
   Json.Decode.succeed (\pid pdate -> {id = pid, date = pdate})
   |> required "id" (jsonDecInt64)
   |> required "date" (Json.Decode.int)

jsonEncEvent : Event -> Value
jsonEncEvent  val =
   Json.Encode.object
   [ ("id", jsonEncInt64 val.id)
   , ("date", Json.Encode.int val.date)
   ]



type alias AccountInfo  =
   { id: Int64
   , email: String
   , full_name: (Maybe String)
   , phone_number: (Maybe String)
   , info_complete: Bool
   }

jsonDecAccountInfo : Json.Decode.Decoder ( AccountInfo )
jsonDecAccountInfo =
   Json.Decode.succeed (\pid pemail pfull_name pphone_number pinfo_complete -> {id = pid, email = pemail, full_name = pfull_name, phone_number = pphone_number, info_complete = pinfo_complete})
   |> required "id" (jsonDecInt64)
   |> required "email" (Json.Decode.string)
   |> fnullable "full_name" (Json.Decode.string)
   |> fnullable "phone_number" (Json.Decode.string)
   |> required "info_complete" (Json.Decode.bool)

jsonEncAccountInfo : AccountInfo -> Value
jsonEncAccountInfo  val =
   Json.Encode.object
   [ ("id", jsonEncInt64 val.id)
   , ("email", Json.Encode.string val.email)
   , ("full_name", (maybeEncode (Json.Encode.string)) val.full_name)
   , ("phone_number", (maybeEncode (Json.Encode.string)) val.phone_number)
   , ("info_complete", Json.Encode.bool val.info_complete)
   ]



type alias LogInSendPasswordForm  =
   { email: String
   }

jsonDecLogInSendPasswordForm : Json.Decode.Decoder ( LogInSendPasswordForm )
jsonDecLogInSendPasswordForm =
   Json.Decode.succeed (\pemail -> {email = pemail})
   |> required "email" (Json.Decode.string)

jsonEncLogInSendPasswordForm : LogInSendPasswordForm -> Value
jsonEncLogInSendPasswordForm  val =
   Json.Encode.object
   [ ("email", Json.Encode.string val.email)
   ]



type alias LogInSendCodeForm  =
   { email: String
   , code: String
   }

jsonDecLogInSendCodeForm : Json.Decode.Decoder ( LogInSendCodeForm )
jsonDecLogInSendCodeForm =
   Json.Decode.succeed (\pemail pcode -> {email = pemail, code = pcode})
   |> required "email" (Json.Decode.string)
   |> required "code" (Json.Decode.string)

jsonEncLogInSendCodeForm : LogInSendCodeForm -> Value
jsonEncLogInSendCodeForm  val =
   Json.Encode.object
   [ ("email", Json.Encode.string val.email)
   , ("code", Json.Encode.string val.code)
   ]



type alias UpdateAccountForm  =
   { full_name: String
   , phone_number: String
   }

jsonDecUpdateAccountForm : Json.Decode.Decoder ( UpdateAccountForm )
jsonDecUpdateAccountForm =
   Json.Decode.succeed (\pfull_name pphone_number -> {full_name = pfull_name, phone_number = pphone_number})
   |> required "full_name" (Json.Decode.string)
   |> required "phone_number" (Json.Decode.string)

jsonEncUpdateAccountForm : UpdateAccountForm -> Value
jsonEncUpdateAccountForm  val =
   Json.Encode.object
   [ ("full_name", Json.Encode.string val.full_name)
   , ("phone_number", Json.Encode.string val.phone_number)
   ]



type alias RsvpInfo  =
   { name: String
   , user_id: Int64
   }

jsonDecRsvpInfo : Json.Decode.Decoder ( RsvpInfo )
jsonDecRsvpInfo =
   Json.Decode.succeed (\pname puser_id -> {name = pname, user_id = puser_id})
   |> required "name" (Json.Decode.string)
   |> required "user_id" (jsonDecInt64)

jsonEncRsvpInfo : RsvpInfo -> Value
jsonEncRsvpInfo  val =
   Json.Encode.object
   [ ("name", Json.Encode.string val.name)
   , ("user_id", jsonEncInt64 val.user_id)
   ]


