module Meetup.Model exposing
    ( Flags
    , Model
    , Msg(..)
    , Page(..)
    , PageModel(..)
    , initPageModel
    )

import Bootstrap.Navbar as Navbar
import Browser exposing (UrlRequest)
import Browser.Navigation as Navigation
import Http
import Meetup.Api as Api
import Meetup.Pages.Account.Model as AccountPage
import Meetup.Pages.Catalog.Model as CatalogPage
import Meetup.Pages.Login.Model as LoginPage
import Meetup.Pages.Meetup.Model as MeetupPage
import Url exposing (Url)


type alias Flags =
    {}


type Msg
    = UrlChange Url
    | ClickedLink UrlRequest
    | NavMsg Navbar.State
    | CatalogPageMsg CatalogPage.Msg
    | MeetupPageMsg MeetupPage.Msg
    | LoginPageMsg LoginPage.Msg
    | AccountPageMsg AccountPage.Msg


type alias Model =
    { navKey : Navigation.Key
    , page : Page
    , navState : Navbar.State
    , pageModel : PageModel
    , redirectOnLogin : Maybe Page
    }


type Page
    = PageHome
    | PageCatalog
    | PageMeetup Int
    | PageLogin
    | PageAccount
    | PageNotFound


type PageModel
    = HomePageModel ()
    | CatalogPageModel CatalogPage.Model
    | MeetupPageModel MeetupPage.Model
    | LoginPageModel LoginPage.Model
    | AccountPageModel AccountPage.Model


initPageModel : Page -> ( PageModel, Cmd Msg )
initPageModel p =
    case p of
        PageHome ->
            ( HomePageModel (), Cmd.none )

        PageCatalog ->
            let
                ( m, cmds ) =
                    CatalogPage.init
            in
            ( CatalogPageModel m, Cmd.map CatalogPageMsg cmds )

        PageMeetup i ->
            let
                ( m, cmds ) =
                    MeetupPage.init i
            in
            ( MeetupPageModel m, Cmd.map MeetupPageMsg cmds )

        PageLogin ->
            ( LoginPageModel LoginPage.init, Cmd.none )

        PageAccount ->
            let
                ( m, cmds ) =
                    AccountPage.init
            in
            ( AccountPageModel m, Cmd.map AccountPageMsg cmds )

        PageNotFound ->
            ( HomePageModel (), Cmd.none )
