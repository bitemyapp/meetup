module Meetup.Pages.Meetup.Model exposing
    ( Model
    , Msg(..)
    , init
    , loadAccountInfo
    , loadMeetup
    , loadMeetupEvents
    , nowCmd
    , update
    )

import Dict exposing (Dict)
import Http
import Meetup.Api as Api
import Meetup.ApiEndpoints as ApiEndpoints
import Meetup.Lib exposing (handleHttpError)
import Task
import Time


type Msg
    = NoOp
    | NeedLogin
    | MeetupLoaded (Result Http.Error Api.Meetup)
    | EventsLoaded (Result Http.Error (List Api.Event))
    | TimeLoaded Time.Posix
    | AccountLoaded (Result Http.Error Api.AccountInfo)
    | Rsvp Api.Event
    | RsvpDone Api.Event (Result Http.Error Api.RsvpInfo)
    | RsvpListLoaded Api.Event (Result Http.Error (List Api.RsvpInfo))
    | UnRsvp Api.Event
    | UnRsvpDone Api.Event (Result Http.Error ())


type alias Model =
    { meetupId : Int
    , errors : List String
    , infos : List String
    , now : Maybe Time.Posix
    , meetup : Maybe Api.Meetup
    , events : List Api.Event
    , account : Maybe Api.AccountInfo
    , eventRsvps : Dict Int (List Api.RsvpInfo)
    }


init : Int -> ( Model, Cmd Msg )
init meetupId =
    ( { meetupId = meetupId
      , errors = []
      , infos = []
      , now = Nothing
      , meetup = Nothing
      , events = []
      , account = Nothing
      , eventRsvps = Dict.fromList []
      }
    , Cmd.batch
        [ loadMeetup meetupId
        , loadMeetupEvents meetupId
        , nowCmd
        , loadAccountInfo
        ]
    )


update : Msg -> Model -> ( Model, List Msg, Cmd Msg )
update msg model =
    case msg of
        NeedLogin ->
            -- handled by parent update
            ( model, [], Cmd.none )

        NoOp ->
            ( model, [], Cmd.none )

        MeetupLoaded (Err e) ->
            handleHttpError e model NeedLogin

        MeetupLoaded (Ok meetup) ->
            ( { model | meetup = Just meetup, errors = [] }, [], Cmd.none )

        EventsLoaded (Err e) ->
            handleHttpError e model NeedLogin

        EventsLoaded (Ok events) ->
            ( { model | events = events, errors = [] }
            , []
            , Cmd.batch (List.map (\ev -> ApiEndpoints.rsvpList (RsvpListLoaded ev) ev.id) events)
            )

        TimeLoaded now ->
            ( { model | now = Just now }, [], Cmd.none )

        AccountLoaded (Err e) ->
            handleHttpError e model NoOp

        AccountLoaded (Ok acc) ->
            ( { model | account = Just acc, errors = [] }, [], Cmd.none )

        Rsvp event ->
            ( model, [], ApiEndpoints.rsvpCreate (RsvpDone event) event.id )

        RsvpDone event (Err e) ->
            handleHttpError e model NeedLogin

        RsvpDone event (Ok rsvp) ->
            let
                upd mOld =
                    case mOld of
                        Nothing ->
                            Just [ rsvp ]

                        Just old ->
                            Just (old ++ [ rsvp ])

                newD =
                    Dict.update event.id upd model.eventRsvps
            in
            ( { model
                | errors = []
                , infos = [ "You've RSVPd successfully" ]
                , eventRsvps = newD
              }
            , []
            , Cmd.none
            )

        UnRsvp event ->
            ( model, [], ApiEndpoints.rsvpDelete (UnRsvpDone event) event.id )

        RsvpListLoaded event (Err e) ->
            handleHttpError e model NeedLogin

        RsvpListLoaded event (Ok rsvps) ->
            let
                newD =
                    Dict.insert event.id rsvps model.eventRsvps
            in
            ( { model | eventRsvps = newD, errors = [] }, [], Cmd.none )

        UnRsvpDone event (Err e) ->
            handleHttpError e model NeedLogin

        UnRsvpDone event (Ok ()) ->
            let
                mAccId =
                    case model.account of
                        Nothing ->
                            Nothing

                        Just acc ->
                            Just acc.id

                upd mOld =
                    case mOld of
                        Nothing ->
                            Nothing

                        Just old ->
                            Just (List.filter (\x -> not (Just x.user_id == mAccId)) old)

                newD =
                    Dict.update event.id upd model.eventRsvps
            in
            ( { model
                | errors = []
                , infos = [ "You've un-RSVPd successfully" ]
                , eventRsvps = newD
              }
            , []
            , Cmd.none
            )


loadMeetup : Int -> Cmd Msg
loadMeetup meetupId =
    ApiEndpoints.loadMeetup MeetupLoaded meetupId


loadMeetupEvents : Int -> Cmd Msg
loadMeetupEvents meetupId =
    ApiEndpoints.loadMeetupEvents EventsLoaded meetupId


loadAccountInfo : Cmd Msg
loadAccountInfo =
    ApiEndpoints.loadAccountInfo AccountLoaded


nowCmd : Cmd Msg
nowCmd =
    Task.perform TimeLoaded Time.now
