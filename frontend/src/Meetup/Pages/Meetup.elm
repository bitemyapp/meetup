module Meetup.Pages.Meetup exposing (view)

import Bootstrap.Alert as Alert
import Bootstrap.Button as Button
import Bootstrap.Card as Card
import Bootstrap.Card.Block as Block
import Dict exposing (Dict)
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Markdown
import Maybe.Extra
import Meetup.Pages.Meetup.Model exposing (..)
import Meetup.Util exposing (..)
import Time exposing (Month(..))


view : Model -> List (Html Msg)
view model =
    let
        titleSuffix =
            model.meetup |> Maybe.map (\meetup -> meetup.title) |> fromMaybe ""

        renderDateStr dt =
            let
                p =
                    Time.millisToPosix dt

                z =
                    Time.utc
            in
            String.fromInt (Time.toDay z p)
                ++ " "
                ++ displayMonth (Time.toMonth z p)
                ++ " "
                ++ String.fromInt (Time.toYear z p)

        futureEvents =
            let
                events =
                    Just (\now -> List.filter (\ev -> ev.date >= Time.posixToMillis now) model.events)
                        |> Maybe.Extra.andMap model.now
                        |> fromMaybe []
                        |> List.sortBy .date

                rsvpButtonBlock event =
                    case model.account of
                        Nothing ->
                            Button.button
                                [ Button.primary
                                , Button.attrs [ onClick NeedLogin ]
                                ]
                                [ text "Log in to RSVP" ]

                        Just acc ->
                            let
                                isRsvpAlready =
                                    List.member acc.id (List.map .user_id (fromMaybe [] (Dict.get event.id model.eventRsvps)))
                            in
                            if isRsvpAlready then
                                Button.button
                                    [ Button.primary
                                    , Button.attrs [ onClick (UnRsvp event) ]
                                    ]
                                    [ text "un-RSVP" ]

                            else
                                Button.button
                                    [ Button.primary
                                    , Button.attrs [ onClick (Rsvp event) ]
                                    ]
                                    [ text "RSVP" ]

                rsvpInfoBlock event =
                    let
                        rsvps =
                            fromMaybe [] (Dict.get event.id model.eventRsvps)

                        renderRsvp rsvp =
                            li [] [ text rsvp.name ]

                        usersNum =
                            List.length rsvps
                    in
                    if usersNum == 0 then
                        text "Nobody RSVPd"

                    else
                        div []
                            [ text <| "Users who RSVPd (" ++ String.fromInt usersNum ++ ")"
                            , ul []
                                (List.map renderRsvp rsvps)
                            ]

                renderEvent ev =
                    Card.config [ Card.attrs [ class "mb-4" ] ]
                        |> Card.block []
                            [ Block.titleH4 [] [ text (renderDateStr ev.date) ]
                            , Block.text [] [ rsvpButtonBlock ev ]
                            , Block.text [] [ rsvpInfoBlock ev ]
                            ]
                        |> Card.view
            in
            [ h2 [] [ text "Future events" ]
            ]
                ++ (case events of
                        [] ->
                            [ text "No future events" ]

                        _ ->
                            List.map renderEvent events
                   )
                ++ [ div [ class "mb-4" ] [] ]

        pastEvents =
            let
                events =
                    Just (\now -> List.filter (\ev -> ev.date < Time.posixToMillis now) model.events)
                        |> Maybe.Extra.andMap model.now
                        |> fromMaybe []
                        |> List.sortBy .date

                renderEvent ev =
                    Card.config []
                        |> Card.block []
                            [ Block.titleH1 [] [ text (renderDateStr ev.date) ] ]
                        |> Card.view
            in
            [ h2 [] [ text "Past events" ]
            ]
                ++ (case events of
                        [] ->
                            [ text "No past events" ]

                        _ ->
                            List.map renderEvent events
                   )
                ++ [ div [ class "mb-4" ] [] ]

        renderMeetup meetup =
            Markdown.toHtml Nothing meetup.description
                ++ futureEvents
                ++ pastEvents
    in
    List.concat
        [ [ h1 [] [ text titleSuffix ] ]
        , List.map (Alert.simpleDanger [] << (\x -> [ text x ])) model.errors
        , List.map (Alert.simpleInfo [] << (\x -> [ text x ])) model.infos
        , model.meetup |> Maybe.map renderMeetup |> fromMaybe []
        ]


displayMonth : Time.Month -> String
displayMonth m =
    case m of
        Jan ->
            "January"

        Feb ->
            "February"

        Mar ->
            "March"

        Apr ->
            "April"

        May ->
            "May"

        Jun ->
            "June"

        Jul ->
            "July"

        Aug ->
            "August"

        Sep ->
            "September"

        Oct ->
            "October"

        Nov ->
            "November"

        Dec ->
            "December"
