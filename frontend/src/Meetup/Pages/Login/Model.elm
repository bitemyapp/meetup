module Meetup.Pages.Login.Model exposing
    ( Model
    , Msg(..)
    , init
    , loadAccountInfo
    , update
    )

import Http
import Meetup.Api as Api
import Meetup.ApiEndpoints as ApiEndpoints
import Meetup.Util exposing (..)


type Msg
    = NoOp
    | GotAccountInfo (Result Http.Error Api.AccountInfo)
    | EnterEmail
    | EmailChange String
    | CodeChange String
    | EnterCode
    | EmailSent (Result Http.Error ())
    | CodeSent (Result Http.Error Api.AccountInfo)
    | FullNameChange String
    | PhoneNumberChange String
    | SaveProfileDetails
    | UpdateAccountDone (Result Http.Error Api.AccountInfo)


type alias Model =
    { errors : List String
    , infos : List String
    , email : String
    , code : String
    , oneTimePassword : String
    , showCodeInput : Bool
    , accInfo : Maybe Api.AccountInfo
    , fullName : String
    , phoneNumber : String
    }


init : Model
init =
    { errors = []
    , infos = []
    , email = ""
    , code = ""
    , oneTimePassword = ""
    , showCodeInput = False
    , accInfo = Nothing
    , fullName = ""
    , phoneNumber = ""
    }


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        NoOp ->
            ( model, Cmd.none )

        EnterEmail ->
            ( model, ApiEndpoints.logInSendPassword EmailSent { email = model.email } )

        EmailChange v ->
            ( { model | email = v }, Cmd.none )

        CodeChange v ->
            ( { model | code = v }, Cmd.none )

        EnterCode ->
            ( model
            , ApiEndpoints.login CodeSent
                { email = model.email
                , code = model.code
                }
            )

        EmailSent (Err e) ->
            ( { model | errors = [ "Error while sending one-time code, please try again" ] }
            , Cmd.none
            )

        EmailSent (Ok ()) ->
            ( { model
                | errors = []
                , infos = [ "One-time code sent successfully. Please check your Spam folder." ]
                , showCodeInput = True
              }
            , Cmd.none
            )

        CodeSent (Err e) ->
            ( { model | errors = [ "Error while trying to log in" ] }
            , Cmd.none
            )

        CodeSent (Ok accInfo) ->
            ( { model
                | errors = []
                , infos = []
                , showCodeInput = False
                , accInfo = Just accInfo
                , phoneNumber = fromMaybe "" accInfo.phone_number
                , fullName = fromMaybe "" accInfo.full_name
              }
            , Cmd.none
            )

        GotAccountInfo (Err e) ->
            let
                err =
                    ( { model | errors = [ "Error while loading account info" ] }, Cmd.none )
            in
            case e of
                Http.BadStatus rsp ->
                    case rsp.status.code of
                        401 ->
                            ( { model | accInfo = Nothing }, Cmd.none )

                        _ ->
                            err

                _ ->
                    err

        GotAccountInfo (Ok accInfo) ->
            ( { model | accInfo = Just accInfo }
            , Cmd.none
            )

        FullNameChange v ->
            ( { model | fullName = v }, Cmd.none )

        PhoneNumberChange v ->
            ( { model | phoneNumber = v }, Cmd.none )

        SaveProfileDetails ->
            ( model
            , ApiEndpoints.updateAccount UpdateAccountDone
                { full_name = model.fullName
                , phone_number = model.phoneNumber
                }
            )

        UpdateAccountDone (Err e) ->
            ( { model | errors = [ "Error while updating account info" ] }
            , Cmd.none
            )

        UpdateAccountDone (Ok accInfo) ->
            -- handled by parent update
            ( { model | accInfo = Just accInfo }
            , Cmd.none
            )


loadAccountInfo =
    ApiEndpoints.loadAccountInfo GotAccountInfo
