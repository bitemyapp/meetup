module Meetup.Pages.Home exposing (view)

import Meetup.Model exposing (..)
import Html exposing (..)
import Markdown


view : List (Html Msg)
view =
    [ h1 [] [ text "Welcome" ]
    , p []
        (Markdown.toHtml Nothing
            """

Welcome to Meedup.Vodka. Open source Elm+Haskell app where you can
organize a meetup with ability for others to RSVP.

Sources can be found at our [GitLab](https://gitlab.com/k-bx/meetup)
                """
        )
    ]
