module Meetup.Pages.Account.Model exposing
    ( Model
    , Msg(..)
    , init
    , loadAccountInfo
    , update
    )

import Http
import Meetup.Api as Api
import Meetup.ApiEndpoints as ApiEndpoints
import Meetup.Lib exposing (handleHttpError)


type Msg
    = NoOp
    | GotAccountInfo (Result Http.Error Api.AccountInfo)
    | NeedLogin


type alias Model =
    { errors : List String
    , accInfo : Maybe Api.AccountInfo
    }


init : ( Model, Cmd Msg )
init =
    ( { errors = []
      , accInfo = Nothing
      }
    , loadAccountInfo
    )


update : Msg -> Model -> ( Model, List Msg, Cmd Msg )
update msg model =
    case msg of
        NeedLogin ->
            -- handled by parent update
            ( model, [], Cmd.none )

        NoOp ->
            ( model, [], Cmd.none )

        GotAccountInfo (Err e) ->
            handleHttpError e model NeedLogin

        GotAccountInfo (Ok accInfo) ->
            if accInfo.info_complete then
                ( { model | accInfo = Just accInfo }
                , []
                , Cmd.none
                )

            else
                ( model
                , [ NeedLogin ]
                , Cmd.none
                )


loadAccountInfo =
    ApiEndpoints.loadAccountInfo GotAccountInfo
