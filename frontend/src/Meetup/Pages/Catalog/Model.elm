module Meetup.Pages.Catalog.Model exposing
    ( Model
    , Msg(..)
    , init
    , update
    )

import Http
import Meetup.Api as Api
import Meetup.ApiEndpoints as ApiEndpoints
import Meetup.Lib exposing (..)


type Msg
    = CatalogLoaded (Result Http.Error (List Api.Meetup))
    | NeedLogin


type alias Model =
    { items : Maybe (List Api.Meetup)
    , errors : List String
    }


init : ( Model, Cmd Msg )
init =
    ( { items = Nothing
      , errors = []
      }
    , ApiEndpoints.loadCatalog CatalogLoaded
    )


update : Msg -> Model -> ( Model, List Msg, Cmd Msg )
update msg model =
    case msg of
        NeedLogin ->
            -- handled by parent update
            ( model, [], Cmd.none )

        CatalogLoaded (Err e) ->
            handleHttpError e model NeedLogin

        CatalogLoaded (Ok items) ->
            ( { model | items = Just items, errors = [] }
            , []
            , Cmd.none
            )
