module Meetup.Pages.Account exposing (view)

import Meetup.Api as Api
import Meetup.Pages.Account.Model exposing (..)
import Meetup.Util exposing (..)
import Bootstrap.Alert as Alert
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)


renderNeedLogin : Html Msg
renderNeedLogin =
    text "Loading..."


renderAccountInfo : Api.AccountInfo -> Html Msg
renderAccountInfo accInfo =
    text <| "Hello, " ++ accInfo.email


view : Model -> List (Html Msg)
view model =
    List.concat
        [ [ h1 [] [ text "Account Info" ] ]
        , List.map (Alert.simpleDanger [] << (\x -> [ text x ])) model.errors
        , [ maybe renderNeedLogin renderAccountInfo model.accInfo ]
        ]
