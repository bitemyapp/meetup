module Meetup.Pages.Catalog exposing
    ( view
    )

import Bootstrap.Alert as Alert
import Bootstrap.Card as Card
import Bootstrap.Card.Block as Block
import Bootstrap.Grid as Grid
import Bootstrap.Grid.Col as Col
import Bootstrap.Grid.Row as Row
import Html exposing (..)
import Html.Attributes exposing (..)
import Http
import Markdown
import Meetup.Api as Api
import Meetup.Pages.Catalog.Model exposing (..)
import Meetup.Util exposing (..)
import Url.Builder as UrlBuilder


view : Model -> List (Html Msg)
view model =
    let
        renderCatalogError e =
            text ("There was an error while loading the catalog: " ++ showHttpError e)

        renderCatalogItem : Api.Meetup -> Card.Config Msg
        renderCatalogItem item =
            Card.config [ Card.attrs [] ]
                |> Card.header []
                    [ img
                        [ class "rounded mx-auto d-block catalog-page-list-item-img"

                        -- , src (Maybe.withDefault "" item.cover_url)
                        ]
                        []
                    ]
                |> Card.block []
                    [ Block.titleH2 [] [ a [ href ("#meetup/" ++ String.fromInt item.id) ] [ text item.title ] ]
                    , Block.text []
                        [ div []
                            [ p [] <| Markdown.toHtml Nothing item.description
                            ]
                        ]
                    ]

        renderDeck xs =
            Grid.row
                []
                (List.map
                    (Grid.col
                        [ Col.xs12
                        , Col.sm12
                        , Col.md6
                        , Col.lg4
                        , Col.xl4
                        , Col.attrs [ class "catalog-page-col" ]
                        ]
                        << List.singleton
                        << Card.view
                    )
                    xs
                )
    in
    List.concat
        [ [ h1 [] [ text "Latest additions" ] ]
        , List.map (Alert.simpleDanger [] << (\x -> [ text x ])) model.errors
        ]
        ++ [ Grid.container []
                (List.map renderDeck
                    [ List.map renderCatalogItem
                        (Maybe.withDefault [] model.items)
                    ]
                )
           ]
