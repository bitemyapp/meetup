module Meetup.Pages.Login exposing (view)

import Meetup.Pages.Login.Model exposing (..)
import Bootstrap.Alert as Alert
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)


loginForm model =
    let
        codeInput =
            [ div [ class "mb-1" ] <|
                [ label [] <|
                    [ div [ class "div-label-for" ]
                        [ text "Code" ]
                    , input
                        [ type_ "text"
                        , class "form-control"
                        , placeholder "Enter code"
                        , value model.code
                        , onInput CodeChange
                        ]
                        []
                    ]
                , span
                    [ class "btn btn-light"
                    , onClick EnterCode
                    ]
                    [ text "Enter code" ]
                ]
            ]
    in
    Html.form [ onSubmit NoOp ]
        [ div [ class "form-group" ] <|
            [ div [ class "mb-1" ] <|
                [ label [] <|
                    [ div [ class "div-label-for" ]
                        [ text "Email address" ]
                    , input
                        [ type_ "email"
                        , class "form-control"
                        , placeholder "Enter email"
                        , value model.email
                        , onInput EmailChange
                        ]
                        []
                    ]
                , span
                    [ class "btn btn-light"
                    , onClick EnterEmail
                    ]
                    [ text "Send password" ]
                ]
            ]
                ++ (if model.showCodeInput then
                        codeInput

                    else
                        []
                   )
        ]


fillAccountForm model accInfo =
    let
        form =
            [ div [ class "mb-1" ] <|
                [ label [] <|
                    [ div [ class "div-label-for" ]
                        [ text "Full Name" ]
                    , input
                        [ type_ "text"
                        , class "form-control"
                        , placeholder "Enter full name"
                        , value model.fullName
                        , onInput FullNameChange
                        ]
                        []
                    ]
                ]
            , div [ class "mb-1" ] <|
                [ label [] <|
                    [ div [ class "div-label-for" ]
                        [ text "Phone Number" ]
                    , input
                        [ type_ "text"
                        , class "form-control"
                        , placeholder "Enter phone name"
                        , value model.phoneNumber
                        , onInput PhoneNumberChange
                        ]
                        []
                    ]
                ]
            , span
                [ class "btn btn-light"
                , onClick SaveProfileDetails
                ]
                [ text "Save" ]
            ]
    in
    [ h1 [] [ text "Complete Account Details" ] ]
        ++ List.map (Alert.simpleDanger [] << (\x -> [ text x ])) model.errors
        ++ List.map (Alert.simpleInfo [] << (\x -> [ text x ])) model.infos
        ++ form


view : Model -> List (Html Msg)
view model =
    let
        signInView =
            [ h1 [] [ text "Sign In" ]
            ]
                ++ List.map (Alert.simpleDanger [] << (\x -> [ text x ])) model.errors
                ++ List.map (Alert.simpleInfo [] << (\x -> [ text x ])) model.infos
                ++ [ loginForm model ]
    in
    case model.accInfo of
        Nothing ->
            signInView

        Just accInfo ->
            fillAccountForm model accInfo
